﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" author="Anton Dimkov">
//  Copyright (c) Anton Dimkov 2012. All rights reserved.
// </copyright>
// <summary>
//  Declaration of the Program class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace IconSet
{
    using System;
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using System.IO;

    using Mono.Options;

    /// <summary>
    /// Declaration of the Program class.
    /// </summary>
    class Program
    {
        private const string InputKey = "input";
        private const string OutKey = "out";
        private const string SizeKey = "size";
        private const string ColumnsKey = "columns";
        private const string HelpKey = "help";
        private const string QietKey = "quiet";

        private const string DefaultOutFileName = "set.png";

        /// <summary>
        /// Mains the specified args.
        /// </summary>
        /// <param name="args">The args.</param>
        static void Main(string[] args)
        {
            var _params = new Dictionary<string, object>();

            var optionParser = InitializeOptionParser(_params);

            try
            {
                optionParser.Parse(args);
                SetupDefaultOptions(_params);
            }
            catch (InputOptionException)
            {
                Console.WriteLine("Missed input parameter");
                optionParser.WriteOptionDescriptions(Console.Error);
                Console.ReadKey(true);
                return;
                
            }
            catch (OptionException)
            {
                optionParser.WriteOptionDescriptions(Console.Error);
                Console.ReadKey(true);
                return;
            }

            if (!_params.ContainsKey(HelpKey))
            {
                var basePath = (string)_params[InputKey];
                var outputFile = (string)_params[OutKey];
                var imageItemSize = (int)_params[SizeKey];
                var columns = (int)_params[ColumnsKey];

                var imageProcessor = new ImageProcessor(imageItemSize);
                var images = Directory.GetFiles(basePath);

                using (var imageSet = imageProcessor.GetImageSet(images, columns))
                {
                    imageSet.Save(Path.Combine(basePath, outputFile), ImageFormat.Png);
                }

                Console.WriteLine(String.Format("Complotted! ImageSet saved into '{0}'.", _params[OutKey]));
            }
            
            if(!(bool)_params[QietKey])
            {
                Console.ReadKey(true);
            }
        }

        /// <summary>
        /// Setups the default options.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        private static void SetupDefaultOptions(IDictionary<string, object> parameters)
        {
            if (!parameters.ContainsKey(SizeKey))
            {
                parameters[SizeKey] = 16;    
            }

            if (!parameters.ContainsKey(ColumnsKey))
            {
                parameters[ColumnsKey] = 10;
            }

            if (!parameters.ContainsKey(QietKey))
            {
                parameters[QietKey] = false;
            }

            if (!parameters.ContainsKey(OutKey))
            {
                if (!parameters.ContainsKey(InputKey) && !parameters.ContainsKey(HelpKey))
                {
                    throw new InputOptionException();
                }

                parameters[OutKey] = Path.Combine((string)parameters[InputKey], DefaultOutFileName);
            }
        }

        /// <summary>
        /// Initializes the option parser.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>Initialized option parser.</returns>
        private static OptionSet InitializeOptionParser(IDictionary<string, object> parameters)
        {
            return new OptionSet
                {
                    {"i=|input=", "Path to directory with images", v => parameters[InputKey] = v},
                    {"o=|out=", "Path to file to save result(default input + imageSet.png)", v => parameters[OutKey] = v},
                    {"s=|size=", "Size of image item(default is 16)", (int v) => parameters[SizeKey] = v},
                    {"c=|columns=", "Count of columns(default is 10)", (int v) => parameters[ColumnsKey] = v},
                    {"q|quiet", "Close agter operation completed", v => parameters[QietKey] = true},
                    {"h|?", "Shows help", (int v) => parameters[HelpKey] = v},
                };
        }
    }
}
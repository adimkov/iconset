﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageProcessor.cs" author="Anton Dimkov">
//  Copyright (c) Anton Dimkov 2012. All rights reserved. 
// </copyright>
// <summary>
//   Declaration of the ImageProcessor class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace IconSet
{
    using System;
    using System.Drawing;

    /// <summary>
    /// Declaration of the MainViewModel class.
    /// </summary>
    public class ImageProcessor
    {
        /// <summary>
        /// The size of image item.
        /// </summary>
        private readonly int _itemSize;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageProcessor"/> class.
        /// </summary>
        /// <param name="itemSize">Size of the item.</param>
        public ImageProcessor(int itemSize)
        {
            _itemSize = itemSize;
        }

        /// <summary>
        /// The get image set.
        /// </summary>
        /// <param name="images">The path to images.</param>
        /// <param name="columnCount">The column count.</param>
        /// <returns>
        /// Image set.
        /// </returns>
        public Image GetImageSet(string[] images, int columnCount)
        {
            var imagesCount = images.Length;
            var actualColumns = 1;
            
            if (imagesCount >= columnCount)
            {
                actualColumns = columnCount;
            }
            
            var actualRowsCount = (imagesCount / columnCount) + 1;

            var iconSetBinmap = new Bitmap(_itemSize * actualColumns, _itemSize * actualRowsCount);

            using (var iconSetGraphic = Graphics.FromImage(iconSetBinmap))
            {
                for (int index = 0; index < imagesCount; index++)
                {
                    using (var image = Image.FromFile(images[index]))
                    {
                        using (var normolizedBitmap = ScaleImage(image))
                        {
                            var row = index / columnCount;
                            var column = index % columnCount;

                            iconSetGraphic.DrawImageUnscaled(normolizedBitmap, column * _itemSize, row * _itemSize);
                        }
                    }
                }
            }

            return iconSetBinmap;
        }

        /// <summary>
        /// Scales the image to fit in specified size.
        /// </summary>
        /// <param name="baseImageItem">
        /// The base image item.
        /// </param>
        /// <returns>
        /// Normalized image size. 
        /// </returns>
////        private static int _num = 0;
        private Image ScaleImage(Image baseImageItem)
        {
            var normolizedBitmap = new Bitmap(_itemSize, _itemSize);
            var graphics = Graphics.FromImage(normolizedBitmap);

            var xPos = 0;
            var yPos = 0;

            var scale = _itemSize / (float)Math.Max(baseImageItem.Width, baseImageItem.Height);

////            baseImageItem.Save(string.Format(@"c:\Temp\icons\work\raw{0}.png", _num), ImageFormat.Png);
            
            if (scale > 1)
            {
                scale = 1;
            }

            var scaledSize = new Size((int)(baseImageItem.Width * scale), (int)(baseImageItem.Height * scale));

            if (scaledSize.Width <= _itemSize)
            {
                xPos = (_itemSize - scaledSize.Width) / 2;
            }

            if (scaledSize.Height <= _itemSize)
            {
                yPos = (_itemSize - scaledSize.Height) / 2;
            }

            graphics.DrawImage(baseImageItem, xPos, yPos, scaledSize.Width, scaledSize.Height);
////            normolizedBitmap.Save(string.Format(@"c:\Temp\icons\work\work{0}.png", _num++), ImageFormat.Png);
            return normolizedBitmap;
        }
    }
}
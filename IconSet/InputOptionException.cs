﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputOptionException.cs" author="Anton Dimkov">
//     Copyright (c) Anton Dimkov 2012. All rights reserved.
// </copyright>
// <summary>
// 
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace IconSet
{
    using Mono.Options;

    public class InputOptionException : OptionException
    {
         
    }
}
# IconSet

Creates IconSet from images placed into directory. Images will be glued in one big image with specified step and size.

Similar images useful in web development or in a project with huge count of icons and images. Specified image can be retreived from IconSet like in coordinate grid.

### Parameters:

i|--input        - path to a directory with images for gluing
o|--output     - path to file to place iconset
c|--columns  - count of columns in iconset. By default 10
s|--size        - size of each icon in set. Icon will be resized to fit in specified bound By default 16px
q|--quiet       - application automaticaly closes after work completed
h|?              - shows this help